# Disable mouse to wake screen

## This script disables mouse wakeup when screen is locked
It assumes your USB mouse is listed as "Mouse" or "mouse" change it accordingly

## How to install ?

sudo apt install xinput

chmod +x disable_mouse_wake.sh

Add to KDE auto start in "System settings" > "Startup and Shutdown" > "Start Automatically" > "Add Script"

## Optionally you can turn off displays right after locking with this:

Go to System Settings > Notifications > Event source "Screen Saver" > "Screen Locked" > "Run Command" and add the following command `/bin/sleep 1 ; /usr/bin/xset dpms force off` 
