#!/bin/bash

# allow only one instance
r=$(pidof -x -o $$ disable_mouse_wake.sh)
set -- $r
if [ "${#@}" -ge 1 ]; then
    echo "Script already running. Exit..."
    exit
fi

ativa_desativa () {
   mouses=$(xinput list | grep 'Mouse\|mouse' | egrep -o 'id=[0-9][0-9]' | sed 's\id=\\')
   for x in $mouses; do
      if $1 ; then
        xinput --set-prop $x "Device Enabled" "1"
      else
        xinput --set-prop $x "Device Enabled" "0"
      fi
   done
}

dbus-monitor --session "type='signal',interface='org.freedesktop.ScreenSaver'" |
  while read x; do
    case "$x" in 
      # You can call your desired script in the following line instead of the echo:
      *"boolean true"*) ativa_desativa false;;
      *"boolean false"*) ativa_desativa true;;  
    esac
done